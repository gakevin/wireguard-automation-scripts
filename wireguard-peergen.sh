#!/bin/bash

# Variables
# ---

#peername=
#peerip=
#peeraddress=
#peerprivkey=
#peerpubkey=
#peerprekey=
#serverpubkey=

# ---

main_function () {
	peername_func && 								# define peername
	mkdir -p /etc/wireguard/peers/$peername &&  # make peer folder
	keygen_func &&                                  # generates keys
	variable_func &&                                # defines dependent variables
	peerconf_func &&                              # make peer.conf file
	addpeer_func &&								# add peer to wg0
	peerqr_func                                   # make qr code  
	echo "Done"
}

# Function Definitions
# ---

peername_func () {
	if [[ $(cat /etc/wireguard/peers/peer01 |& grep -o "No such file")  == "No such file" ]]
	then
		peername=peer01
	else
		x=$(ls /etc/wireguard/peers/ | grep peer | tail -1 | sed 's/peer//') &&
        	x=$(( 10#$x + 1 )) &&
		if [ "$x" -lt "10" ]
		then
			peername=peer0$x
		else
			peername=peer$x
		fi
	fi
}

keygen_func () {
	umask 077; wg genkey | tee /etc/wireguard/peers/$peername/$peername.private.key | wg pubkey > /etc/wireguard/peers/$peername/$peername.public.key
	umask 077; wg genpsk > /etc/wireguard/peers/$peername/$peername.preshared.key
}

variable_func () {
	peerip=$(echo $peername | sed 's/peer0//' | echo ""$(($(cat /dev/stdin) + 1))"")
	peerprivkey=$(cat /etc/wireguard/peers/$peername/$peername.private.key)
	peerpubkey=$(cat /etc/wireguard/peers/$peername/$peername.public.key)
        peerprekey=$(cat /etc/wireguard/peers/$peername/$peername.preshared.key)
        serverpubkey=$(cat /etc/wireguard/server/server.public.key)
	peerconf=(/etc/wireguard/peers/$peername/$peername.conf)
}

peerconf_func () {
	echo "[Interface]"												>> $peerconf
	echo "Address = 192.0.2.$peerip, 2001:db8:1::$peerip"			>> $peerconf
	echo "PrivateKey = $peerprivkey"								>> $peerconf
	echo "ListenPort = 51820"										>> $peerconf
	echo "DNS = 1.1.1.1, 1.0.0.1"									>> $peerconf
	echo ""															>> $peerconf
	echo "[Peer]"													>> $peerconf
	echo "PublicKey = $serverpubkey"								>> $peerconf
	echo "PresharedKey = $peerprekey"								>> $peerconf
	echo "Endpoint = bluepod.cloud:51820"							>> $peerconf
	echo "AllowedIPs = 0.0.0.0/0, ::/0"								>> $peerconf
	echo "PersistentKeepalive = 20"									>> $peerconf
}


addpeer_func () {
	echo "add $peername automatically?: "
	read response1
	if  [[ $response1 == yes ]] ;
	then
    		peeraddress=$(cat /etc/wireguard/peers/$peername/$peername.conf | grep  "Address = " | sed 's/Address = //g' | sed 's/\/[0-9][0-9]//g' | sed 's/ //g') &&
    		wg set wg0 peer $peerpubkey preshared-key /etc/wireguard/peers/$peername/$peername.preshared.key allowed-ips $peeraddress &&
    		wg-quick save wg0 &&
    		sed -i '/Endpoint = /d' /etc/wireguard/wg0.conf &&
    		wg syncconf wg0 <(wg-quick strip wg0)
		echo "$peername has been set and config reload!"
	fi
}


peerqr_func () {
	qrencode -t png -o /etc/wireguard/peers/$peername/$peername.png -r /etc/wireguard/peers/$peername/$peername.conf
	echo "do you want to display th qr code: "
	read response2
	if [[ $response2 == yes ]]
	then
		qrencode -t ansiutf8 < /etc/wireguard/peers/$peername/$peername.conf
		echo "$peername"
	fi
}

# ---

main_function
