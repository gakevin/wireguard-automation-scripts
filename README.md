# WireGuard Automation Scripts

## Intro
This is where I will start to publish scripts I have made related to wireguard deployment on rhel servers.

## Support
You can reach me at gakevin@protonmail.com or on Matrix(Element) at @gakevin:matrix.org

## Roadmap

- peer unset
- wg0 initialization
- key rotation
- ansible playbook

## Contributing

feel free to contribute

## License

 GNU GENERAL PUBLIC LICENSE Version 2

## Project status

Active
